<?php 

function getLetterGrade ($grade){
if ($grade < 75) {
    return "$grade is equivalent to D";
}
else if ($grade <= 76 ){
    return "$grade is equivalent to C-";
}
else if ($grade <= 79 ){
    return "$grade is equivalent to C";
}
else if ($grade <= 82 ){
    return "$grade is equivalent to C+";
}
else if ($grade <= 85 ){
    return "$grade is equivalent to B-";
}
else if ($grade <= 88 ){
    return "$grade is equivalent to B";
}
else if ($grade <= 91 ){
    return "$grade is equivalent to B+";
}
else if ($grade <= 94 ){
    return "$grade is equivalent to A-";
}
else if ($grade <= 97 ){
    return "$grade is equivalent to A";
}
else if ($grade <= 100 ){
    return "$grade is equivalent to A+";
}
else{
    return 'Please enter a valid grade.';
}
}